const game = new Game();

function showHorses() {
  game.horseArray.forEach(horse => console.log(horse.name));
}

function showAccount() {
  console.log(game.player.count);
}

function setWager(horse, count) {
  if (count > game.player.count) {
    throw new Error('У вас недостаточно денег');
  } else {
    game.wagerArray.push({ horse, count });
    game.player.count -= count;
  }
}

function startRacing() {
  const promiseArray = [];
  game.horseArray.forEach((horse, i) => {
    promiseArray.push(horse.run());
    promiseArray[i].then(raceInfo => console.log(`${raceInfo.horseName} - ${raceInfo.raceTime.toFixed(0)}`));
  });

  let winner = '';
  let winCount = 0;
  Promise.race(promiseArray).then(raceInfo => winner = raceInfo.horseName);
  Promise.all(promiseArray).then(() => {
    game.wagerArray.forEach(wager => {
      if (wager.horse === winner) {
        winCount = wager.count * 2;
        game.player.count += winCount;
      }
    });
    if (winCount ) {
      console.log(`Вы выиграли ${winCount}, текущий счет: ${game.player.count}`);
    } else {
      console.log(`Текущий счет: ${game.player.count}`);
    }
    game.wagerArray = [];
  });
}

function newGame() {
  const startCount = prompt('Начальный баланс', 100);
  game.player.count = Number(startCount);
  console.clear();
}

function Game() {
  this.wagerArray = [];
  this.horseArray = [];
  this.player = new Player(100);

  this.horseArray.push(new Horse('Буцефал'));
  this.horseArray.push(new Horse('Молния'));
  this.horseArray.push(new Horse('Сахарок'));
  this.horseArray.push(new Horse('Гром'));
  this.horseArray.push(new Horse('Картошка'));

  function Horse(name) {
    this.name = name;
    this.run = function() {
      const horseName = this.name;
      return new Promise(resolve => {
        const raceTime = Math.random() * (3000 - 500) + 500;
        setTimeout(() => resolve({ horseName, raceTime }), raceTime);
      });
    };
  }

  function Player(startCount) {
    this.count = startCount;
  }
} 
